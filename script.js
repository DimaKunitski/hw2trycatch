const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const root = document.createElement('div');
const ul = document.createElement('ul');

document.body.appendChild(root);
root.setAttribute('id', 'root');
root.appendChild(ul);

books.forEach((el) => {
    const li = document.createElement('li');

    try {
        if (!el.author) throw new SyntaxError('Неполные данные: нет автора.');
        if (!el.name) throw new SyntaxError('Неполные данные: нет названия книги');
        if (!el.price) throw new SyntaxError('Неполные данные: нет цены');

        li.appendChild(document.createTextNode(`author: ${el.author} name: "${el.name}" price: ${el.price}`));
        ul.appendChild(li);
    } catch (err) {
        console.log(err.message);
    }
});



